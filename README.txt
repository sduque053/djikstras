This project consists of the implementation of Djikstras algorithm in C++. It
has been implemented with 3 major classes in mind: Graphs, priority queues, and
shortest path.

The graph class is built with a matrix representation, which allows to store
both wether there is a path aswell as the path cost inside the same varibale. 
However for low density graphs this is no ideal. The graph has a default value
for testing purposes, however when initialized with size density and max path 
cost it starts on zeros, and can the be randomly filled. Also the << operator
has been overloaded to handle graph printing.

The priority queue is built with addres encapsulation, where the last item of
the structure points to element. It is based around a priority queue template
that can be found online but it has been heavy altered in order to get most of
the functions required to work for the algorithm. Originally it only contained
inserting items, deletion of first item, and wether an item was in the queue.

The path class was originally implemented with a graph element inside it. 
However i thought it may be unnecesary and waste to much memory for larger
graphs. Instead the graph is passed by reference and with that the shortes path
between any 2 nodes is calculated.

Santiago Duque Villegas