#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

char V2C[] = {'A','B','C','D','E','F','G','S','T'};
             //0,  1,  2,  3,  4,  5,  6,  7,  8


int main(){

  vector< vector<int> > graph={
		 {0,2,9,0,0,0,0},//A
		 {2,0,0,4,5,1,0},//B
		 {9,0,0,3,1,6,0},//C
		 {0,4,3,0,0,0,3},//D
		 {0,5,1,0,0,0,2},//E
		 {0,1,6,0,0,0,7},//F
		 {0,0,0,3,2,7,0},//G
  };    //A,B,C,D,E,F,G
  
  vector < vector <int> > open;
  vector < vector <int> > closed;
  vector < vector <int> >::iterator IT;

  int a, m, FINISH, START;
  bool flag1, flag2;

  cout<<"Punto de partida: ";
  cin>>START;
  cout<<endl;

  cout<<"Punto de llegada: ";
  cin>>FINISH;
  cout<<endl;

  closed.push_back({START,0});
  int NODE = closed[0][0];
  int Curr_Dist = closed[0][1];
  flag2=false;
  

  //Something to chose the next node [x][0]

  while(flag2==false){
  
      for(int i=0;i<9;i++){
  
	  if(graph[NODE][i]>0){	//Revisar si hay una conexion

	      flag1=false;

	      for(int k=0;k<closed.size();k++){
		  if(closed[k][0]==i){
		      flag1=true;
		      break;
		  }
	      }

	      if(flag1==false){
	      
		  for(int j=0;j<open.size();j++){			//Revisar si hay una conexion menor

		      if(open[j][0]==i){
			  if(graph[NODE][i]+Curr_Dist<open[j][1]){
			      flag1=true;
			      open[j]={i,graph[NODE][i]+Curr_Dist};
			  }else
			      flag1=true;
		      }
		  }
	      }
		  
	      if(flag1==false){
		  open.push_back({i,graph[NODE][i]+Curr_Dist});  //a�adir/actualizar conexion
	      }
	  }
      }
      
      a=open[0][1];
      m=0;
      for(int i=1;i<open.size();i++){	//revisar menor camino desde este nodo
	  if(open[i][1]<a){
	      m=i;
	      a=open[i][1];
	  }
      }
  
      closed.push_back(open[m]);
      NODE=open[m][0];
      Curr_Dist=open[m][1];

      IT=open.begin();
  
      open.erase(open.begin()+m);

      
      for(int i=0;i<closed.size();i++){
	  if(closed[i][0]==FINISH){
	      flag2=true;
	      break;
	  }
      }
      if(flag2==false&&open.size()==0){
	  cout<<"No path found."<<endl;
	  return 0;

      }


  
  }

  cout<<"CLOSED"<<endl;
  for(int j=0;j<closed.size();j++){
      cout<<closed[j][0]<<closed[j][1];
      cout<<" "<<endl;
  }
  cout<<"OPEN"<<endl;
  for(int j=0;j<open.size();j++){
      cout<<open[j][0]<<open[j][1];
      cout<<" "<<endl;
  }
  cout<<endl;
  
  return 0;
}
