#include <iostream>
#include "Path.hpp"

std::vector<std::vector<int>> pathdjik::path(graph& gr, int s, int f){

    cleanup();

    int currnt_node,currnt_dist; //to keep track of position in algorithm
    int open_weit; //stores the edge value currently in queue of the destination that is being checked
    bool flag1, flag2;//flags to determine when certain things are done
    node vertex; //Stores the values of the top of the queue

    PriorityQ open;//queue of all the nodes on the open set

    open.printQ();

    std::vector<int> edges;//stores the edges of the currently checked node

    closed.push_back({s, s, 0}); //Pushes the starting node to the definitive path
    currnt_node=closed[0][1];
    currnt_dist=closed[0][2];

    flag2=false; //flag2 determines when the algorithms is finished

    std::cout<<"Running Djikstras\n";


    while (flag2==false){
        
        edges=gr.read_node(currnt_node);
        
        for(int i=0;i<edges.size();i++){ //Checks all the edges of the current node
            
            open_weit=open.containd(i);
            if(edges[i]>0){
                flag1=false;
                for(int k=0;k<closed.size();k++){ //Checks if the edge is another in the closed set
		            if(closed[k][1]==i){
                        flag1=true;
                        break;
		            }
	            }
                if((flag1==false)&&((open_weit==0)||(open_weit>edges[i]))){
                    open.insert(edges[i]+currnt_dist,currnt_node,i); //If the edge is smaller than the current one or not in the open set, add it
                }
            }
        }
        vertex=open.top(); //takes the first item in the queue and adds it to the closed set

        closed.push_back({vertex.originnode,vertex.destinationnode,vertex.weight});
        currnt_node=vertex.destinationnode;
        currnt_dist=vertex.weight;
        
        open.pop(); //Removes it from the queue

        for(int i=0;i<closed.size();i++){ //Checks if the finish is in the closed set
	        if(closed[i][1]==f){
	            flag2=true;
	        }
        }

        if(flag2==false&&open.sizeq()==0){ //If the open set is empty and the finish is not in the closed set there is no path
            std::cout<<"No path available\n";
            closed.resize(1);
            closed={{0,0,0}};
            return closed;
        }

    }
    
    std::cout<<"CLOSED"<<std::endl;

    for(int j=0;j<closed.size();j++){ //Prints the closed set
        std::cout<<closed[j][0]<<"-"<<closed[j][2]<<"-"<<closed[j][1];
        std::cout<<" "<<std::endl;
    }

    return closed;

}

int pathdjik::pcost(){
    /*if(closed.size()==0){
        std::cout<<"Djikstras algorithm not yet run"; //If the algorithms hasnt run the cost is not known
    }else if(closed[closed.size()-1][2]==0){
        std::cout<<"Path not found";//if the last item of the closed set has a zero cost theres no solution
    }else{
        std::cout<<"Path cost: "<<closed[closed.size()-1][2]; //returns the path cost to destination stored in the closed vector
    }*/

    return closed[closed.size()-1][2];

}

/*void pathdjik::add(const graph map, int size){

    gr.resize(size);

    gr=map;

    std::cout<<"Loaded graph with: \n"<<gr<<"\n";
}*/

void pathdjik::cleanup(){

    closed.erase(closed.begin(),closed.end());

}