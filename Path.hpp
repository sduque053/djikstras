#include <vector>
#include <iostream>
#include "graph.hpp"
#include "PrrtyQ.hpp"

class pathdjik{

    public:

    std::vector<std::vector<int>> path(graph& gr, int s, int f); //calculate djikstras shortest path between s and f on graph gr

    int pcost(); //returns the final path cost of djikstras

    //void add(const graph map, int size);  //Stores a graph of size size in the variable gr

    void cleanup(); //deletes the currently stored shortest path, is also run when computing a new shortest path

    private:

    //graph gr;
    std::vector<std::vector<int>> closed; //Shortest path
};