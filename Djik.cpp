#include <iostream>
#include <vector>
#include <cstdlib>

#include "Path.hpp"


int main(void){

    srand(time(NULL));

    int size;
    int dens; //In percentage
    int cost;
    int storecost=0;

    std::cout<<"Insert number of nodes: ";
    std::cin>>size;
    std::cout<<"\n";

    std::cout<<"Insert edge density in %: ";
    std::cin>>dens;
    std::cout<<"\n";

    std::cout<<"Insert maximun edge cost: ";
    std::cin>>cost;
    std::cout<<"\n";

    graph diagram(size,dens,cost);
    diagram.fill();

    std::cout<<"Graph: \n"<<diagram<<"\n";

    pathdjik shortest;


    for(int i=1;i<size;i++){
        shortest.path(diagram, 0, i);

        storecost+=shortest.pcost();

        std::cout<<"Iteration number: "<<i<<"\n";

    }

    std::cout<<"\nTotal average path cost: "<<storecost<<"\n";

    return 0;
}
