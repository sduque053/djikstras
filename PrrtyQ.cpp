#include "PrrtyQ.hpp"

//Class priority queue methods implementations
PriorityQ::PriorityQ(){ //Starts the queue empty

    q = NULL; //NULL is used to denote the end of the queue
}

void PriorityQ::printQ(){
    n *ptr;
    ptr = q;
    if (q == NULL) //First item of queue is end of queue
      std::cout << "Queue is empty\n";
    else
    {
      std::cout << "Queue is :\n";
      std::cout << "Priority Item\n";
      while (ptr != NULL)
      {
        std::cout << ptr->originnode << "-" << ptr->weight << "-" << ptr->destinationnode << std::endl; //Prints every item of the queue
        ptr = ptr->next; //Iterates through items of queue
      }
    }
}

void PriorityQ::insert(int w, int o, int d){
    n *t, *c;   //defines two pointers of type n
    t = new n; //assigns memory to the variable t
    t->originnode = o;
    t->destinationnode = d;
    t->weight = w; //fills the variable with the new data
        
    if (q == NULL || w < q->weight){//if the queue is empty or if the item has higher priority than the firs item 
        t->next = q; //point to the first item as the next in line
        q = t; //replace q with new first item
    }
    else{
        c = q;//creates an iterator
        while (c->next != NULL && c->next->weight <= w)
            c = c->next;//iterates until priority is bigger or is the end of the queue
        t->next = c->next; //Point to next item in iterator
        c->next = t; //replace with temporal variable
    }
}

void PriorityQ::pop(){
    n *t;
    if (q == NULL) //Empty queue
      return;
    else{
        t=q;//keeps current pointer of q
        q = q->next;//makes the first item in queue the second one
        free(t); //deletes the old first pointer
    }
}

void PriorityQ::chng(int nw, int o, int d){

    n *copy,*prev; //Creates axuiliary variables
    
    if((q->originnode==o)&&(q->destinationnode==d)){//if the item is in first position use pop and reinsert
        pop();
        insert(nw,o,d);
        return;
    }

    copy=q->next; //points to next item 
    prev=q; //points to current item

    while(copy!=NULL){

        if((copy->originnode==o)&&(copy->destinationnode==d)){
            prev->next=copy->next; //Instead of prev pointing to to copy it points to the one after it, making it inaccesible
            free(copy); //frees the memory ocupied by copy
            insert(nw,o,d);//reinserts 
            return;
        }
        
        prev=copy;
        copy=copy->next;//iterates through all items of the queue
    }

    return;

}

void PriorityQ::chngo(int nw, int o, int d){ //See chng

    n *copy,*prev;
    
    if((q->originnode==o)){
        pop();
        insert(nw,o,d);
        return;
    }

    copy=q->next;
    prev=q;

    while(copy!=NULL){

        if((copy->originnode==o)){
            prev->next=copy->next;
            free(copy);
            insert(nw,o,d);
            return;
        }
        
        prev=copy;
        copy=copy->next;
    }

    return;

}

void PriorityQ::chngd(int nw,int o, int d){ //See chng

    n *copy,*prev;
    
    if((q->destinationnode==d)){
        pop();
        insert(nw,o,d);
        return;
    }

    copy=q->next;
    prev=q;

    while(copy!=NULL){

        if((copy->destinationnode==d)){
            prev->next=copy->next;
            free(copy);
            insert(nw,o,d);
            return;
        }
        
        prev=copy;
        copy=copy->next;
    }

    return;

}

node PriorityQ::top(){ //returns the value of q in a struct type that does not contain a pointer to the next item
    
    node read;

    if(q==NULL){

        

    }else{
        
        read.originnode=q->originnode;
        read.destinationnode=q->destinationnode;
        read.weight=q->weight;
        return read;

    
    }
    
    return read;

}

int PriorityQ::contain(int o, int d){
    
    n *c; //Pointer to iterate through queue

    c=q;

    if (c == NULL)//If queue is empty return 0/or no edge
      return 0;

    while(c!=NULL){
        if((c->originnode==o)&&(c->destinationnode==d)){
            return c->weight; //if both information value correspond then return the weight of that edge
        }
        c=c->next;//Iterate through all of the items of queue
    }

    return 0; //If is not in the queue return 0/or no edge
}

int PriorityQ::containo(int o){ //See contain
    n *c;

    c=q;
    if (c == NULL){
      return 0;
    }
    while(c!=NULL){
        if((c->originnode==o)){
            return c->weight;
        }
        c=c->next;
    }

    return 0;
}

int PriorityQ::containd(int d){ //See contain
    n *c;

    c=q;
    if (c == NULL){
      return 0;
    }
    while(c!=NULL){
        if((c->destinationnode==d)){
            return c->weight;
        }
        c=c->next;
    }

    return 0;
}

int PriorityQ::sizeq(){
    
    n *c; //pointer to iterate through queue

    int count;

    if(q==NULL)
        return 0;//if queue empty size 0

    count=1;//at least one item in queue
    c=q;

    while(c->next!=NULL){
        count++;
        c=c->next; //iterate trough queue and count every item
    }

    return count; //Return size
}
