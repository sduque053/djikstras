#include "graph.hpp"

//Function implementations

void graph::resize(int ns){ //Resizes the graph, and the resizes the number of connections of each node using the vector functions
    map.resize(ns);

    for(int i=0;i<size;i++){
            
        map[i].resize(ns);

    }
}

void graph::fill(){ //Fills the graph with random numbers

    for(int i=0;i<size;i++){
                        
        for(int j=0;j<size;j++){
            if(i==j){
                map[i][j]=0; //There are no connections along the diagonal because a node doesnt connecto to itself
            }else if(rand()%100<density){ //if the random value is within the density it adds a connection of value between 1 and cst
                map[i][j]=map[j][i]=(rand()%(cst-1))+1;
            }else
                map[i][j]=map[j][i]=0;//If its not the there is no connection
        }
    }

}
void graph::empty(){ //deletes all edges by assigning connections a value of 0/disconected

    for(int i=0;i<size;i++){
                        
        for(int j=0;j<size;j++){

            map[i][j]=0;

        }
    }

}

graph::graph(int node, int den, int cost){
    size=node; //number of nodes or vertex, both dimensions of the matrix
    density=den; //edge density
    cst=cost; //max of travel cost of the edge

    map.resize(node); //resizes the number of nodes

    for(int i=0;i<size;i++){
            
        map[i].resize(node); //resizes the number of edges of every node

        for(int j=0;j<size;j++){

            map[i][j]=0; //starts the graphs in zeros

        }

    }
    std::cout<<"Empty graph of:"<<size<<" nodes created."<<"\n";

}

graph::graph(){ //Creates the default graph, used for testing
    size=7;
    cst=9;
    density=45;
    std::cout<<"Default graph created."<<"\n";
}


std::vector<int> graph::read_node(int n){ //Returns the vector of edges of the desired node
    return map[n];
}

int graph::read_weight(int n, int m){ //Returns the travel cost of the edge between n and m
    return map[n][m];
}

int graph::read_size(){ //Returns the number of nodes in the graph
    return map.size();
}

void graph::write_weight(int n, int m, int w){ //Changes the travel cost of a given edge

    map[n][m]=w;

}

std::ostream& operator<<(std::ostream& os, const graph& gr){ //Overloads the << to print the graph in matrix form
    
    for(int i=0;i<gr.size;i++){
        os<<"[ ";
        for(int j=0;j<gr.size;j++){
            os<<" "<<gr.map[i][j];
        }
        os<<" ]\n";
    }
    return os;
}
