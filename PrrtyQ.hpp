#include <iostream>
#include <cstdlib>


//This priority queue implementation is based around an online example, but built upon
struct node{
  int weight;
  int originnode;
  int destinationnode;
};

struct n // node declaration
{
  int weight;
  int originnode;
  int destinationnode;
  struct n *next;
};

class PriorityQ{

    public:
    
    PriorityQ();

    void printQ();                      //prints queue
    
    void insert(int w, int o, int d);   //inserts item
    void pop();                         //removes first item of queue
    void chng(int nw, int o, int d);    //changes an item priority and rearenges it
    void chngo(int nw, int o, int d);    //changes an item priority and rearenges it
    void chngd(int nw, int o, int d);    //changes an item priority and rearenges it



    node top();                          //returns the first item of queue
    int contain(int o, int d);           //returns wether info exists in queue
    int containo(int o);                 //returns wether the origin exists in the queue
    int containd(int d);                 //returns wether the destination exists in the queue
    int sizeq();                         //returns the number of elements in the queue                  

    private:
    
    n *q;

}; 