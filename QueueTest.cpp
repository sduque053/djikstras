#include <iostream>
#include "PrrtyQ.hpp"

int main(){

    PriorityQ Que;

    std::cout<<Que.sizeq()<<"\n";

    Que.insert(4,1,5);

    Que.printQ();

    Que.insert(1,2,0);

    Que.insert(9,1,3);

    if(Que.contain(2,0))
        std::cout<<"Si esta\n";

    if(not Que.contain(1,4))
        std::cout<<"no esta\n";


    Que.chng(6,1,5);

    Que.printQ();

    Que.pop();

    Que.printQ();

    Que.chng(11,1,5);

    Que.printQ();

    return 0;
}