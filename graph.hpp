#include <iostream>
#include <cstdlib>
#include <vector>

class graph{

    public:

    graph(int node, int den, int cost);//Initialize an undirected graph with a number of vertex equal to node, edge density den, and max edge cost of cost
    graph();        //Initialize an undirected graph with predetermined values, for testing purposes
    
    void resize(int ns);    //change the number of nodes to ns
    void fill();    //Randomly assign edges with random costs, up to "cost"
    void empty();   //Delete all edges

    std::vector<int> read_node(int n);  //Returns the relation of node with all other nodes
    int read_weight(int n, int m);  //Retruns the value asociated with an edge
    int read_size();    //Returns the number of vertex

    void write_weight(int n, int m, int w); //Sets the value associated with an edge

    friend std::ostream& operator<<(std::ostream& os, const graph& gr);//Operator overloading for printing

    private:

    int size;
    int density;
    int cst;
    std::vector< std::vector<int> > map={
		 {0,2,9,0,0,0,0},//A
		 {2,0,0,4,5,1,0},//B
		 {9,0,0,3,1,6,0},//C
		 {0,4,3,0,0,0,3},//D
		 {0,5,1,0,0,0,2},//E
		 {0,1,6,0,0,0,7},//F
		 {0,0,0,3,2,7,0},//G
    };  //A,B,C,D,E,F,G


};